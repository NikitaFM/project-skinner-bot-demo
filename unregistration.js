var SteamCommunity = require("steamcommunity");
var SteamTotp = require("steam-totp");
var ReadLine = require("readline");
var fs = require("fs");

var community = new SteamCommunity();
var rl = ReadLine.createInterface({
  "input": process.stdin,
  "output": process.stdout
});

rl.question("Username: ", function(accountName) {
  rl.question("Password: ", function(password) {
    var code = SteamTotp.generateAuthCode("AXMfLo/YkUjIB9vo0oD4uaX3ImA=");
    community.login({
      accountName: accountName,
      password: password,
      twoFactorCode: code
    }, function(err, sessionID, cookies, steamguard) {
      rl.question("Revocation code: ", function(revocationCode) {
        community.disableTwoFactor(revocationCode, function (error) {
          console.log(arguments);
        });
      });
    });
  });
});

var code = SteamTotp.generateAuthCode("AXMfLo/YkUjIB9vo0oD4uaX3ImA=");