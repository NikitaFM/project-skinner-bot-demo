var path = require("path");
var fs = require("fs");
var express = require("express");
var socketio = require("socket.io");
var SteamUser = require("steam-user");
var TradeOfferManager = require("steam-tradeoffer-manager");
var SteamTotp = require("steam-totp");
var SteamCommunity = require("steamcommunity");

var app = express();
var io = socketio(3003);

var settings;
process.argv.forEach(function (argument) {
  var option = argument.split("=");
  var name = option[0];
  var value = option[1];
  switch (name) {
    case "--bot": {
      try {
        settings = JSON.parse(fs.readFileSync(path.join(__dirname, value), "utf-8"));
      }
      catch (error) {
        console.log(error, "Some error of parse " + value + " file");
      }
      break;
    }
  }
});

if (!settings) console.log("To run server pass your settings options json file to --bot argument, like: node start --bot=\"filename.json\"");

var botData = settings.bot;

console.log(botData);

try {
  var bot = new SteamUser();
}
catch (error) {
  console.log(error);
}

bot.setOptions({ promptSteamGuardCode: false });

var code = SteamTotp.generateAuthCode(botData.shared_secret);

// setInterval(function () {
//   console.log("chaitalshah", SteamTotp.generateAuthCode("k0jb2Ubu0rZquI70F5vbsdGUFLA="));
//   console.log("avnishaggarwal", SteamTotp.generateAuthCode("WBDRF5OFDxCEDYnPy9biUAJXY10="));
//   console.log("raveesornnil", SteamTotp.generateAuthCode("D1nt0Z4nZoAjQfViAmGhcV/3iYw="));
// }, 5000);

bot.logOn({
  accountName: botData.accountName,
  password: botData.password,
  twoFactorCode: code
});

bot.on("webSession", function (sessionID, cookies) {
  tradeOfferManager.setCookies(cookies, function (err) {
    if (err) {
      console.log("Error in running trade bot " + botData.accountName + ". Error: " + err);
    }
    else {
      console.info("Trade bot " + botData.accountName + " running successfully.");
    }
  });
});

bot.on("accountLimitations", function (limited, communityBanned, locked, canInviteFriends) {
  if (limited) {
    console.log("Our account is limited. We cannot send friend invites, use the market, open group chat, or access the web API.");
  }
  if (communityBanned){
    console.log("Our account is banned from Steam Community");
  }
  if (locked){
    console.log("Our account is locked. We cannot trade/gift/purchase items, play on VAC servers, or access Steam Community.  Shutting down.");
  }
  if (!canInviteFriends){
    console.log("Our account is unable to send friend requests.");
  }
});

var tradeOfferManager = new TradeOfferManager({
  steam: bot,
  language: "en",
  pollInterval: 5000,
  cancelTime: 300000
});

// setTimeout(function () {
//   tradeOfferManager.loadInventory(730, 2, true, function (err, botInventory) {
//     var offer = tradeOfferManager.createOffer("76561198183318802");
//     offer.addMyItems(botInventory);
//     offer.send("Get items request.", "ZOVC5Jw5", function (err, status) {
//       if (Boolean(err)) {
//         console.log("err", err);
//       }
//       else {
//         console.log("status", status);
//       }
//     });
//   });
// }, 5000);

var community = new SteamCommunity();

 //community.login({
 //  accountName: botData.accountName,
 //  password: botData.password,
 //  twoFactorCode: code
 //}, function (error) {
 //  if (error) {
 //    console.log("#1", error);
 //  }
 //  else {
 //    var time = SteamTotp.time(1000000);
 //    var key = SteamTotp.getConfirmationKey("/3NmfkxaIp6y9Gq3eKuOjNfvvb8=", time, "conf");
 //    community.getConfirmations(time, key, function (err, confirmations) {
 //      if (error) {
 //        console.log("#2", error);
 //      }
 //      else {
 //        console.log(confirmations);
 //        // confirmations.forEach(function (confirmation, id) {
 //        //   console.log(id, confirmation);
 //        //   community.getConfirmationOfferID(confirmation.id, time, confirmation.key, function () {
 //        //     console.log(arguments);
 //        //   });
 //        // });
 //        // var confirmation = confirmations[0];
 //        // console.log(confirmation);
 //        // community.getConfirmationOfferID(confirmation.id, time, key, function () {
 //        //   console.log(arguments);
 //        // });
 //      }
 //    });
 //  }
 //});

tradeOfferManager.on("sentOfferChanged", function (offer, oldState) {
  switch (offer.state) {
    case 3:
      io.sockets.emit("deposit.success", {
        id: offer.id,
        state: offer.state,
        partnerId: offer.partner.getSteamID64(),
        itemsToReceive: offer.itemsToReceive
      });
  }
});

tradeOfferManager.on("sentOfferCanceled", function (offer) {
  io.sockets.emit("deposit.canceled", {
    id: offer.id,
    state: offer.state,
    partnerId: offer.partner.getSteamID64(),
    itemsToReceive: offer.itemsToReceive
  });
});

var global = require(path.join(__dirname, "api/global.js"));
var user = require(path.join(__dirname, "api/user.js"));
var inventory = require(path.join(__dirname, "api/inventory.js"));
var sell = require(path.join(__dirname, "api/sell.js"));
var bought = require(path.join(__dirname, "api/bought.js"));
app.use(global({ tradeOfferManager: tradeOfferManager }));
app.use(user({ community: community }));
app.use(inventory({ tradeOfferManager: tradeOfferManager }));
app.use(sell({ tradeOfferManager: tradeOfferManager }));
app.use(bought({ tradeOfferManager: tradeOfferManager }));

app.get("/", function (req, res) {
  res.send("It's work");
});

process.on("uncaughtException", function(mainError) {
  var mainErrorMessage = __filename + ": " + mainError.toString();
  console.error(mainErrorMessage);
});

app.listen(3002, function () {
  console.info("Server is start on port " + 3002);
});