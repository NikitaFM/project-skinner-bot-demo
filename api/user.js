var express = require("express");
var SteamCommunity = require("steamcommunity");

module.exports = function (options) {

  var Router = express.Router;
  var user = new Router();
  var SteamID = SteamCommunity.SteamID;
  var community = options.community;

  user.get("/user-summaries/:steamId", function(req, res) {
    var steamId = req.params.steamId;
    if (Boolean(steamId)) {
      var sid = new SteamID(steamId);
      community.getSteamUser(sid, function (err, user) {
        res.send(user);
      });
    }
  });

  return user;

};