var express = require("express");

module.exports = function (options) {

  var Router = express.Router;
  var sell = new Router();

  var tradeOfferManager = options.tradeOfferManager;

  sell.get("/sell", function(req, res) {
    var token = req.query.token;
    var sallerId = req.query.sallerId;
    var items = req.query.items;
    if (token && sallerId && items) {
      var offer = tradeOfferManager.createOffer(sallerId);
      offer.addTheirItems(items);
      offer.send("Deposit items request.", token, function (err, status) {
        if (err) {
          console.log("Something went wrong when trying to send the trade offer " + offer.id + ". Steam message: " + err);
          res.status(406).send({
            id: offer.id,
            state: offer.state,
            partnerId: offer.partner.getSteamID64(),
            itemsToReceive: offer.itemsToReceive
          });
        }
        else if (status == "pending"){
          console.log("Trade offer " + offer.id + " sent but awaiting email confirmation. You should probably turn off email confirmation here: http://steamcommunity.com/my/edit/settings/");
          res.status(202).send({
            id: offer.id,
            state: offer.state,
            partnerId: offer.partner.getSteamID64(),
            itemsToReceive: offer.itemsToReceive
          });
        }
        else {
          console.log("Trade offer " + offer.id + " sent successfully");
          res.status(200).send({
            id: offer.id,
            state: offer.state,
            partnerId: offer.partner.getSteamID64(),
            itemsToReceive: offer.itemsToReceive
          });
        }
      });
    }
    else {
      res.status(400).send("Data was wrong.")
    }
  });

  return sell;

};