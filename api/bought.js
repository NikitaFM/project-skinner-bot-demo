var express = require("express");
var _ = require("underscore");

module.exports = function (options) {

  var Router = express.Router;
  var bought = new Router();

  var tradeOfferManager = options.tradeOfferManager;

  bought.get("/bought-trade", function(req, res) {
  var ownerSteamId = req.query.ownerSteamId;
  var ownerToken = req.query.token;
  var boughtItems = req.query.items;
  tradeOfferManager.loadInventory(730, 2, true, function (err, botInventory) {
    // var assetsItems = [ ];
    // boughtItems.forEach(function (boughtItem) {
    //   var existedItem = _.find(botInventory, function (botInventoryItem) {
    //     return boughtItem.classid === botInventoryItem.classid && boughtItem.instanceid === botInventoryItem.instanceid;
    //   });
    //   // 0 - ready to get
    //   // 1 - can't find in inventory
    //   if (!existedItem) {
    //     var notExistedItem = _.pick(boughtItem, "appid", "contextid", "classid", "assetid");
    //     notExistedItem.status = 0;
    //     assetsItems.push(notExistedItem);
    //   }
    //   else {
    //     var readyItem = _.pick(existedItem, "appid", "contextid", "classid", "assetid");
    //     readyItem.status = 1;
    //     assetsItems.push(readyItem);
    //   }
    // });
    // {
    //   assetid,
    //   appid,
    //   contextid,
    //   amount
    // }
    // boughtItem = [ {

    // } ];
    // console.log(boughtItems);
    console.log(ownerSteamId);
    var offer = tradeOfferManager.createOffer(ownerSteamId);
    offer.addMyItems(boughtItems);
    offer.send("Get items request.", ownerToken, function (err, status) {
    if (err) {
      console.log("Something went wrong when trying to send the trade offer " + offer.id + ". Steam message: " + err);
      res.status(406).send({
      id: offer.id,
      state: offer.state,
      partnerId: offer.partner.getSteamID64(),
      itemsToReceive: offer.itemsToReceive
      });
    }
    else if (status == "pending"){
      console.log("Trade offer " + offer.id + " sent but awaiting email confirmation. You should probably turn off email confirmation here: http://steamcommunity.com/my/edit/settings/");
      res.status(202).send({
      id: offer.id,
      state: offer.state,
      partnerId: offer.partner.getSteamID64(),
      itemsToReceive: offer.itemsToReceive
      });
    }
    else {
      console.log("Trade offer " + offer.id + " sent successfully");
      res.status(200).send({
      id: offer.id,
      state: offer.state,
      partnerId: offer.partner.getSteamID64(),
      itemsToReceive: offer.itemsToReceive
      });
    }
    });
  });
  });

  return bought;

};