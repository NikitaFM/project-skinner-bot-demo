var express = require("express");

module.exports = function (options) {

  var Router = express.Router;
  var inventory = new Router();

  var tradeOfferManager = options.tradeOfferManager;

  inventory.get("/bot-inventory", function(req, res) {
    tradeOfferManager.loadInventory(730, 2, true, function (err, botInventory) {
      res.send(botInventory);
    });
  });

  inventory.get("/user-inventory", function(req, res) {
    var steamId = req.query.steamId;
    tradeOfferManager.loadUserInventory(steamId, 730, 2, true, function (err, userInventory) {
      res.send(userInventory);
    });
  });

  return inventory;

};