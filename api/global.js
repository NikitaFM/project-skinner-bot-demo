var express = require("express");
var _ = require("underscore");

module.exports = function (options) {

  var Router = express.Router;
  var global = new Router();

  var tradeOfferManager = options.tradeOfferManager;

  global.get("/exist", function(req, res) {
    var buyItems = req.query.items;
    tradeOfferManager.loadInventory(730, 2, true, function (err, botInventory) {
      var assetsItems = [ ];
      buyItems.forEach(function (buyItem) {
        var existedItem = _.find(botInventory, function (botInventoryItem) {
          return botInventoryItem.classid === buyItem.classid && botInventoryItem.instanceid === buyItem.instanceid;
        });
        if (!existedItem) {
          var notExistedItem = _.pick(buyItem, "classid", "instanceid");
          notExistedItem.status = 0;
          assetsItems.push(notExistedItem);
        }
        else {
          var readyItem = _.pick(existedItem, "classid", "instanceid");
          readyItem.status = 1;
          assetsItems.push(readyItem);
        }
      });
      res.send(assetsItems);
    });
  });

  return global;

};